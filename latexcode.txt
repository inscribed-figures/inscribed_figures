\documentclass[10pt]{beamer}

\usepackage{utopia} %font utopia imported


% set colors
\definecolor{myNewColorA}{RGB}{62, 123, 200}

\setbeamercolor*{palette tertiary}{bg=myNewColorA, fg = white}
\setbeamercolor*{titlelike}{fg=myNewColorA}
\setbeamercolor*{title}{bg=myNewColorA, fg = white}
\usefonttheme{professionalfonts}
% \usepackage{natbib}
% \usepackage{hyperref}
% 	
\setbeamerfont{title}{size=\large}
\setbeamerfont{subtitle}{size=\small}
\setbeamerfont{author}{size=\small}
\setbeamerfont{date}{size=\small}
\setbeamerfont{institute}{size=\small}
\title[]{INSCRIBED FIGURES}

\author[]{PRIYANKA IPPALA : 21B01A1259 :IT-A\\ \vspace{4} GUNDABATTULA MADHURI : 21BO1A1258: IT-A\\ \vspace{4} PERURI SRI VEDHA : 21B01A12E1 : IT-C\\ \vspace{4} DAMMALA MADHUNIKA : 21B01A0206 : EEE\\ \vspace{4} YANDRA PURNA VIJAYA KUMARI : 21B01A54C7 : AI&DS-B}

\date[]
{MARCH 25, 2023}

\AtBeginSection[]{
\begin{frame}
\vfill
\centering
\begin{beamercolorbox}[sep=8pt,center,shadow=true,rounded=true]{title}
\usebeamerfont{title}\insertsectionhead\par%
\end{beamercolorbox}
\vfill
\end{frame}
}
% 	

\begin{document}

%The next statement creates the title page.
\frame{\titlepage}
\begin{frame}
\frametitle{Contents}
\tableofcontents
\end{frame}
% 	
\section{INTRODUCTION}
\begin{frame}{INTRODUCTION: }
\begin{itemize}
\item Given a square of length 'n' and four equal circles of radius 'r' inscribed in it where each circle is located in each corner and tangent to two sides of the square. we have to calculate the area of the largest circle or square that lies completely within the large square and intersects all the four circles in atmost four points.
\end{itemize}
\end{frame}


\section{APPROACH FOR CIRCLE}
\begin{frame}{If the shape is circle:}
\begin{wrapfigure}{\hspace{10} }{\textwidth}
\includegraphics[width=5cm]{cir}
\caption{}
\label{fig:wrapfig}
\end{wrapfigure}
\begin{itemize}
\hspace{17}
\end{itemize}
\end{frame}




\section{APPROACH FOR SQUARE}
\begin{frame}{If the shape is square:}
\begin{wrapfigure}{\hspace{10} }{\textwidth}
\includegraphics[width=5cm]{sq}
\caption{}
\label{fig:wrapfig}
\end{wrapfigure}
\begin{itemize}
\hspace{17}
\end{itemize}
\end{frame}




\section{LEARNINGS}
\begin{frame}{PACKAGES:}
\begin{itemize}
\item Math Module: we have imported math module in this project. The math module is a built in module that contains a set of math methods and constants. we have used the math
 
method "math.sqrt()" to return the square root of a number and the math constant "math.pi" to return the value of pi.
\end{itemize}
\begin{itemize}
\item Sys Module: we have imported sys module and used it's variable "sys.argv" which is a list of command line arguments. In this project, length, radius and shape are passed as command line arguments.
\end{itemize}
\end{frame}


\section{CHALLENGES}
\begin{frame}{CHALLENGES:}
\begin{itemize}
\item If the given diameter of the circle exceeds the
given length of the large square, then all the circles get overlapped and there will be no inscribed circle or square that intersects all the four circles in atmost four points.
\end{itemize}
\begin{itemize}
\item To overcome this, we have given the condition that the diameter of the circle should be less than the length of the large square
\end{itemize}
\end{frame}


\section{}
\begin{frame}{}
\begin{wrapfigure}{\hspace{10} }{\textwidth}
\includegraphics[width=5cm]{overlap.jpg}
\caption{}
% \label{fig:wrapfig}
\end{wrapfigure}
\end{frame}


\section{STATISTICS}
\begin{frame}{STATISTICS: }
\begin{itemize}
\item Number of lines of code : 35
\end{itemize}
\begin{itemize}
\item Number of functions : 4 \\ \vspace{3}
\hspace{5} diagonal( ) \\ \vspace{1} \hspace{5} findarea( ) \\ \vspace{1} \hspace{5} areaCircle( ) \\ \vspace{1} \hspace{5} squarearea( )
\end{itemize}
\end{frame}


\section{DEMO}
\begin{frame}{PROGRAM:}
\includegraphics[width=1.0\textwidth]{Screenshot (42).png}
\end{frame}
\begin{frame}{OUTPUT:}
\includegraphics[width=1.0\textwidth]{out.jpg}
 
\end{frame}





\section*{}
\begin{frame}
\textcolor{myNewColorA}{\Huge{\centerline{Thank you!}}}
\end{frame}

\end{document}
